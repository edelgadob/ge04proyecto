package es.unex.giis.asee.appdoption.ui.userdata;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.UserFragmentViewModel;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View mView;
    private User mUser;

    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.user_view, container, false);
        EditText name = (EditText) mView.findViewById(R.id.editUserName);
        EditText email = (EditText) mView.findViewById(R.id.editUserEmail);
        EditText tel = (EditText) mView.findViewById(R.id.editUserPhone);
        EditText loc = (EditText) mView.findViewById(R.id.editUserLocation);
        EditText pass = (EditText) mView.findViewById(R.id.editUserPassword);

        AppContainer appContainer = ((MyApplication)getActivity().getApplication()).appContainer;
        UserFragmentViewModel userVModel = new ViewModelProvider(this,appContainer.userFactory).get(UserFragmentViewModel.class);
        mUser = userVModel.getUser();

        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                name.setText(mUser.getName());

                email.setText(mUser.getEmail());

                tel.setText(mUser.getTelephone());

                loc.setText(mUser.getAddress().getAddress1());

                pass.setText(mUser.getPassword());
            }
        });



        Button cancelBtn = (Button) mView.findViewById(R.id.cancel_button);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = UserFragmentDirections.actionNavUserToNavHome();
                ((Navigate)getContext()).navigateTo(action);
            }
        });

        Button confirmBtn = (Button) mView.findViewById(R.id.confirm_button);
        confirmBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String ucontactold = mUser.getEmail();
                Address add = new Address(loc.getText().toString(), mUser.getAddress().getCity(), mUser.getAddress().getState(), mUser.getAddress().getPostcode(), mUser.getAddress().getCountry());
                User userN = new User(mUser.getId(), name.getText().toString(), email.getText().toString(), pass.getText().toString(), tel.getText().toString(), add);
                userVModel.updateUser(userN);
                userVModel.updateAll(userN.getEmail(),ucontactold);

                NavDirections action = UserFragmentDirections.actionNavUserToNavHome();
                ((Navigate) getContext()).navigateTo(action);
            }
        });


        return mView;
    }
}