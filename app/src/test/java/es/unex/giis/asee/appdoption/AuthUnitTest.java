package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

public class AuthUnitTest {
    @Test
    public void getToken() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Auth instance = new es.unex.giis.asee.appdoption.Model.Auth();
        final Field field = instance.getClass().getDeclaredField("accessToken");
        field.setAccessible(true);
        field.set(instance, "1x791y3bs823jd3");

        //when
        final String result = instance.getToken();

        //then
        assertEquals("field wasn't retrieved properly", result, "1x791y3bs823jd3");
    }
}
