package es.unex.giis.asee.appdoption.roomdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import es.unex.giis.asee.appdoption.Model.User;


@Dao
public interface UserDAO {
    @Insert
    public long insert(User user);

    @Query("SELECT * FROM users LIMIT 1")
    public User getUser();

    @Update
    public void updateUser(User user);

    @Query("DELETE FROM users WHERE id=:id")
    void delete(long id);

    @Query("DELETE FROM users")
    void deleteAllForTest();
}
