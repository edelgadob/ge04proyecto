package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.ViewModel;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class UpdDelFragmentViewModel extends ViewModel {
    private final PetsRepository mRepository;


    public UpdDelFragmentViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;

    }

    public void updateAnimal(Animal a){
        mRepository.updateAnimal(a);
    }

    public void deleteAnimal(long aid){
        mRepository.deleteAnimal(aid);
    }
}
