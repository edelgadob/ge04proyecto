package es.unex.giis.asee.appdoption.datamanagement;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.roomdb.AnimalDAO;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;
import es.unex.giis.asee.appdoption.roomdb.FavoriteDAO;


public class PetsRepository {
    private static PetsRepository sInstance;
    private AnimalDAO petsDAO;
    private FavoriteDAO favDAO;
    private User mUser;
    private PetNetworkDataSource petsDataSource;
    private AppExecutors mExecutors = AppExecutors.getInstance();
    private LiveData<List<Animal>> homePets= null;
    private LiveData<List<Animal>> userPets=null;
    private LiveData<List<Favorite>> favouritesPets=null;
    private Context context=null;
    Long lastFetchTimeMillis = System.currentTimeMillis();
    Long lastUpdateTimeMillisMap = null;
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 60000;


    private PetsRepository(AnimalDAO dao, FavoriteDAO favdao, PetNetworkDataSource dataSource, Context context){
        this.petsDAO=dao;
        this.favDAO=favdao;
        this.petsDataSource=dataSource;
        this.context=context;
        petsDataSource.fetchAnimals();
        homePets = petsDataSource.getCurrentAnimals();
        homePets.observeForever(newPetsFromNetwork -> {
            mExecutors.diskIO().execute(()->petsDAO.bulkInsert(newPetsFromNetwork));
        });
       mExecutors.diskIO().execute(()->{
            mUser = AppDoptionDatabase.getInstace(context).getUserDAO().getUser();
            userPets=dao.getUserAnimals(mUser.getEmail());
            favouritesPets=favdao.getAllFavs(mUser.getId());
        });

    }


    public synchronized static PetsRepository getInstance(AnimalDAO dao, FavoriteDAO favdao, PetNetworkDataSource dSource, Context context){
        if(sInstance==null){
            sInstance=new PetsRepository(dao,favdao,dSource,context);
        }
        return sInstance;
    }


    public LiveData<List<Animal>> getHomePets(){
      mExecutors.diskIO().execute(()->{
          if(isFetchNeeded()){
              doFetchAnimals();
          }
      });
      return homePets;
    }

    public LiveData<List<Animal>> getUserPets(){
        return userPets;
    }

    public LiveData<List<Favorite>> getFavPets(){return favouritesPets;}

    public void doFetchAnimals(){
        mExecutors.diskIO().execute(() -> {
            petsDAO.deleteAll(mUser.getEmail());
            petsDataSource.fetchAnimals();
            lastUpdateTimeMillisMap= System.currentTimeMillis();
        });
    }



    private boolean isFetchNeeded() {
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        // Implement cache policy: When time has passed or no repos in cache
        return petsDAO.getNumberTotalAnimals() == 0 || timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS;
    }


    public void insertAnimal(Animal a){
        mExecutors.diskIO().execute(()->{
            petsDAO.insert(a);
        });
    }

    public void updateAnimal(Animal a){
        mExecutors.diskIO().execute(()->{
            petsDAO.update(a);
        });
    }

    public void deleteAnimal(long id){
        mExecutors.diskIO().execute(()->{
            petsDAO.delete(id);
        });
    }

    public int getFav(long aid){
        final int[] total = new int[1];
        Log.i("USUARIO",mUser.getName());
       mExecutors.diskIO().execute(()->{
          total[0]=favDAO.getFav(mUser.getId(),aid);
       });
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        Log.i("TOTAL ANIMALS",String.valueOf(total[0]));
       return total[0];
    }

    public void insertFavourite(Favorite fav){
        mExecutors.diskIO().execute(()->{
            fav.setUserid(mUser.getId());
            Log.i("USERID",String.valueOf(fav.getUserid()));
            favDAO.insert(fav);
        });
    }

    public void  deleteFavourite(long aid) {
        mExecutors.diskIO().execute(()->{
            favDAO.delete(mUser.getId(),aid);
        });
    }

    public User getUser(){
        return mUser;
    }

    public void updateUser(User user){
        mExecutors.diskIO().execute(()->{
            AppDoptionDatabase.getInstace(context).getUserDAO().updateUser(user);
            mUser=AppDoptionDatabase.getInstace(context).getUserDAO().getUser();
        });
    }

    public void updateUserAnimals(String new_email, String old_email){
        mExecutors.diskIO().execute(()->{
            petsDAO.updateAll(new_email,old_email);
        });
    }
}

