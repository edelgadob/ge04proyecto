package es.unex.giis.asee.appdoption.Factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giis.asee.appdoption.ViewModels.MyPetsViewModel;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class MyPetsHomeViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final PetsRepository mRepository;

    public MyPetsHomeViewModelFactory(PetsRepository repository) {
        this.mRepository = repository;
    }


    /**
     * Debe de sobrescribir el metodo create para así crear el ViewModel concreto.
     * @param modelClass
     * @param <T> Clase a generar, debe de heredar de ViewModel
     * @return ViewModel generado
     */
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new MyPetsViewModel(mRepository);
    }
}
