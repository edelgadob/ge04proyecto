package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

public class BreedsUnitTest {

    @Test
    public void setPrimary() throws NoSuchFieldException, IllegalAccessException {
        String value = "primary breed";
        es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds("primary_breed", true);
        instance.setPrimary(value);
        final Field field = instance.getClass().getDeclaredField("primary");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getPrimary() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        final Field field = instance.getClass().getDeclaredField("primary");
        field.setAccessible(true);
        field.set(instance, "primary breed");

        //when
        final String result = instance.getPrimary();

        //then
        assertEquals("field wasn't retrieved properly", result, "primary breed");
    }

    @Test
    public void setSecondary() throws NoSuchFieldException, IllegalAccessException {
        String value = "secondary breed";
        es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        instance.setSecondary((Object) value);
        final Field field = instance.getClass().getDeclaredField("secondary");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), (Object) value);
    }

    @Test
    public void getSecondary() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        final Field field = instance.getClass().getDeclaredField("secondary");
        field.setAccessible(true);
        field.set(instance, "secondary breed");

        //when
        final Object result = instance.getSecondary();

        //then
        assertEquals("field wasn't retrieved properly", result, (Object) "secondary breed");
    }

    @Test
    public void setMixed() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        instance.setMixed(value);
        final Field field = instance.getClass().getDeclaredField("mixed");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getMixed() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        final Field field = instance.getClass().getDeclaredField("mixed");
        field.setAccessible(true);
        field.set(instance, false);

        //when
        final boolean result = instance.getMixed();

        //then
        assertEquals("field wasn't retrieved properly", result, false);
    }

    @Test
    public void setUnknown() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = false;
        es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        instance.setUnknown(value);
        final Field field = instance.getClass().getDeclaredField("unknown");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getUnknown() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.Breeds instance = new es.unex.giis.asee.appdoption.Model.Breeds();
        final Field field = instance.getClass().getDeclaredField("unknown");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getUnknown();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }
}
