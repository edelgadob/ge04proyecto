
package es.unex.giis.asee.appdoption.Model;



import androidx.room.Embedded;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Contact implements Serializable
{

    @Ignore
    private long id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    @Embedded
    private Address address;

    private final static long serialVersionUID = -5284287460524553188L;



    public Contact( String email, String phone, Address address) {
        this.email = email;
        this.phone = phone;
        this.address=address;
    }

    @Ignore
    public Contact() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
