package es.unex.giis.asee.appdoption;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class CreatePetUITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void createPetUITest() {
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab), isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.animal_name), isDisplayed()));
        appCompatEditText.perform(replaceText("Test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.animal_breed),isDisplayed()));
        appCompatEditText2.perform(replaceText("t"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.animal_age), isDisplayed()));
        appCompatEditText3.perform(replaceText("t"), closeSoftKeyboard());

        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.animal_description),isDisplayed()));
        textInputEditText.perform(replaceText("t"), closeSoftKeyboard());

        ViewInteraction materialButton = onView(allOf(withId(R.id.send_btn),isDisplayed()));
        materialButton.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.petList)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction editText = onView(
                allOf(withId(R.id.animal_name_delete), isDisplayed()));
        editText.check(matches(withText("Test")));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        editText2.check(matches(withText("Adoptable")));

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        editText3.check(matches(withText("Adoptable")));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.fab),isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.animal_name),isDisplayed()));
        appCompatEditText4.perform(replaceText("Test2"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.animal_breed),isDisplayed()));
        appCompatEditText5.perform(replaceText("w"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.animal_age),isDisplayed()));
        appCompatEditText6.perform(replaceText("w"), closeSoftKeyboard());

        ViewInteraction textInputEditText2 = onView(
                allOf(withId(R.id.animal_description),isDisplayed()));
        textInputEditText2.perform(replaceText("w"), closeSoftKeyboard());

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.send_btn), isDisplayed()));
        materialButton2.perform(click());

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.petList)));
        recyclerView2.perform(actionOnItemAtPosition(1, click()));

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.animal_name_delete),isDisplayed()));
        editText4.check(matches(withText("Test2")));

        ViewInteraction editText5 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        editText5.check(matches(withText("Adoptable")));
    }

    @AfterClass
    public static void restoreDB(){
        AppDoptionDatabase.getInstace(InstrumentationRegistry.getInstrumentation().getContext()).getAnimalDAO().deleteAllForTest();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
