package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Address;

public class UserUnitTest {

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException {
        long value = 123456789;
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User(987654321, "User name","user@gmail.com", "user_password", "927123456", null);
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User("User name","user@gmail.com", "user_password", "927123456", null);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 123456789);

        //when
        final long result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 123456789);
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException {
        String value = "nombre usuario";
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "nombre usuario");

        //when
        final String result = instance.getName();

        //then
        assertEquals("field wasn't retrieved properly", result, "nombre usuario");
    }

    @Test
    public void setEmail() throws NoSuchFieldException, IllegalAccessException {
        String value = "email usuario";
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        instance.setEmail(value);
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getEmail() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "email usuario");

        //when
        final String result = instance.getEmail();

        //then
        assertEquals("field wasn't retrieved properly", result, "email usuario");
    }

    @Test
    public void setPassword() throws NoSuchFieldException, IllegalAccessException {
        String value = "password usuario";
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        instance.setPassword(value);
        final Field field = instance.getClass().getDeclaredField("password");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getPassword() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        final Field field = instance.getClass().getDeclaredField("password");
        field.setAccessible(true);
        field.set(instance, "password usuario");

        //when
        final String result = instance.getPassword();

        //then
        assertEquals("field wasn't retrieved properly", result, "password usuario");
    }

    @Test
    public void setTelephone() throws NoSuchFieldException, IllegalAccessException {
        String value = "927463298";
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        instance.setTelephone(value);
        final Field field = instance.getClass().getDeclaredField("telephone");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getTelephone() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        final Field field = instance.getClass().getDeclaredField("telephone");
        field.setAccessible(true);
        field.set(instance, "927463298");

        //when
        final String result = instance.getTelephone();

        //then
        assertEquals("field wasn't retrieved properly", result, "927463298");
    }

    @Test
    public void setAddress() throws NoSuchFieldException, IllegalAccessException {
        Address value = new Address("Calle Suecia", "Cáceres", "España", "10002", "España");
        es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        instance.setAddress(value);
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getAddress() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.User instance = new es.unex.giis.asee.appdoption.Model.User();
        final Address address = new Address("Calle Suecia", "Cáceres", "España", "10002", "España");
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        field.set(instance, address);

        //when
        final Address result = instance.getAddress();

        //then
        assertEquals("field wasn't retrieved properly", result, address);
    }



}
