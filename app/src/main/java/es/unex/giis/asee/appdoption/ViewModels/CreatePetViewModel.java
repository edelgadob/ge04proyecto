package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.ViewModel;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class CreatePetViewModel extends ViewModel {
    private final PetsRepository mRepository;


    public CreatePetViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;
    }

    public void insertAnimalDB(Animal a){mRepository.insertAnimal(a);}
}
