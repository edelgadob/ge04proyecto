package es.unex.giis.asee.appdoption;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Contact;
import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.Favorite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FavoriteUnitTest {
    @Test
    public void setFidTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 1234567890;
        Favorite instance = new Favorite(523698741L, null);
        instance.setFid(value);
        final Field field = instance.getClass().getDeclaredField("fid");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), 1234567890L);
    }

    @Test
    public void setUseridTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 523698741;
        Favorite instance = new Favorite(0L, null);
        instance.setUserid(value);
        final Field field = instance.getClass().getDeclaredField("userid");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), 523698741L);
    }

    @Test
    public void setAnimalfavTest() throws NoSuchFieldException, IllegalAccessException {
        Address address = new Address("Avda. Universidad", "Caceres", "Caceres", "10003", "Espania");
        Contact contact = new Contact("test@test.com", "987654321", address);
        Animal value = new Animal(10, "105", "Pastor Aleman", "Perro", "2", "Macho", "50cm", "Rudolf", "Es un perro bonito", "94680", "Adoptable", "24/11/2021", "24/11/2021", contact,"foto.png");
        Favorite instance = new Favorite(523698741L, null);
        instance.setAnimalfav(value);
        final Field field = instance.getClass().getDeclaredField("animalfav");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getFidTest() throws NoSuchFieldException, IllegalAccessException {
        final Favorite instance = new Favorite();
        final Field field = instance.getClass().getDeclaredField("fid");
        field.setAccessible(true);
        field.set(instance, 1234567890L);

        //when
        final long result = instance.getFid();

        //then
        assertEquals("field wasn't retrieved properly", result, 1234567890L);
    }

    @Test
    public void getUseridTest() throws NoSuchFieldException, IllegalAccessException {
        final Favorite instance = new Favorite();
        final Field field = instance.getClass().getDeclaredField("userid");
        field.setAccessible(true);
        field.set(instance, 523698741L);

        //when
        final long result = instance.getUserid();

        //then
        assertEquals("field wasn't retrieved properly", result, 523698741L);
    }

    @Test
    public void getAnimalfavTest() throws NoSuchFieldException, IllegalAccessException {
        Address address = new Address("Avda. Universidad", "Caceres", "Caceres", "10003", "Espania");
        Contact contact = new Contact("test@test.com", "987654321", address);
        Animal value = new Animal(10, "105", "Pastor Aleman", "Perro", "2", "Macho", "50cm", "Rudolf", "Es un perro bonito", "94680", "Adoptable", "24/11/2021", "24/11/2021", contact,"foto.png");

        final Favorite instance = new Favorite(1234567890L, 52698741L, null);
        final Field field = instance.getClass().getDeclaredField("animalfav");
        field.setAccessible(true);
        field.set(instance, value);

        //when
        final Animal result = instance.getAnimalfav();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }
}
