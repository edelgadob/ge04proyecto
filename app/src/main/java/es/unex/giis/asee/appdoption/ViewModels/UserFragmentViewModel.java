package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.ViewModel;

import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class UserFragmentViewModel extends ViewModel {
    private final PetsRepository repository;

    public UserFragmentViewModel(PetsRepository repository) {
        this.repository = repository;
    }

    public User getUser(){
        return repository.getUser();
    }


    public void updateUser(User user){
        repository.updateUser(user);
    }


    public void updateAll(String new_mail,String old_mail){
        repository.updateUserAnimals(new_mail,old_mail);
    }
}
