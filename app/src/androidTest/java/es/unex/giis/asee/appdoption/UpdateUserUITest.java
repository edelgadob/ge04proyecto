package es.unex.giis.asee.appdoption;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class UpdateUserUITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    public static User user;

    @BeforeClass
    public static void obtainUser(){
        Address address=new Address("Escuela Politecnica","Caceres", "Caceres","10002","Caceres");
        user = new User("Roberto","admin@admin.com","12345","564923289",address);
    }

    @Test
    public void updateUserUITest() {
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"), isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction navigationMenuItemView = onView(
                allOf(withId(R.id.nav_user), isDisplayed()));
        navigationMenuItemView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.editUserName), isDisplayed()));
        appCompatEditText.perform(replaceText("Lucia"));

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.editUserName), isDisplayed()));
        appCompatEditText2.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.editUserEmail), isDisplayed()));
        appCompatEditText3.perform(replaceText("luciarol@admin.com"));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.editUserEmail), isDisplayed()));
        appCompatEditText4.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.editUserPhone), isDisplayed()));
        appCompatEditText5.perform(replaceText("987654321"));

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.editUserPhone), isDisplayed()));
        appCompatEditText6.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.editUserLocation), isDisplayed()));
        appCompatEditText7.perform(replaceText("Escuela de Derecho"));

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.editUserLocation), isDisplayed()));
        appCompatEditText8.perform(closeSoftKeyboard());

        ViewInteraction materialButton = onView(
                allOf(withId(R.id.confirm_button), isDisplayed()));
        materialButton.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Open navigation drawer"), isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction navigationMenuItemView2 = onView(
                allOf(withId(R.id.nav_user), isDisplayed()));
        navigationMenuItemView2.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.editUserName), isDisplayed()));
        editText.check(matches(isDisplayed()));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.editUserEmail), isDisplayed()));
        editText2.check(matches(isDisplayed()));

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.editUserPhone), isDisplayed()));
        editText3.check(matches(withText("987654321")));

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.editUserLocation), isDisplayed()));
        editText4.check(matches(withText("Escuela de Derecho")));

        ViewInteraction editText6 = onView(
                allOf(withId(R.id.editUserName), isDisplayed()));
        editText6.check(matches(withText("Lucia")));

        ViewInteraction editText7 = onView(
                allOf(withId(R.id.editUserEmail), isDisplayed()));
        editText7.check(matches(withText("luciarol@admin.com")));
    }

    @AfterClass
    public static void restoreDB(){
        AppDoptionDatabase.getInstace(InstrumentationRegistry.getInstrumentation().getContext()).getUserDAO().deleteAllForTest();
        AppDoptionDatabase.getInstace(InstrumentationRegistry.getInstrumentation().getContext()).getUserDAO().insert(user);
    }


    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
