package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class FavouritesViewModel extends ViewModel {
    private final PetsRepository mRepository;
    private final LiveData<List<Favorite>> mUserFavorites;


    public FavouritesViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;
        this.mUserFavorites=mRepository.getFavPets();
    }

    public LiveData<List<Favorite>> getUserFavsPets(){
        return mUserFavorites;
    }
}
