package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.junit.Test;

import java.lang.reflect.Field;

public class AddressUnitTest {

    @Test
    public void setAddress1Test() throws NoSuchFieldException, IllegalAccessException {
        String value = "Avenida Salamanca";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address(value,"Caceres","Extremadura","10001","Spain");
        instance.setAddress1(value);
        final Field field = instance.getClass().getDeclaredField("address1");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }


    @Test
    public void setAddress2Test() throws NoSuchFieldException, IllegalAccessException {
        Object value = "Calle Leo";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        instance.setAddress2(value);
        final Field field = instance.getClass().getDeclaredField("address2");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }


    @Test
    public void setCityTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Caceres";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        instance.setCity(value);
        final Field field = instance.getClass().getDeclaredField("city");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setStateTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Extremadura";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        instance.setState(value);
        final Field field = instance.getClass().getDeclaredField("state");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setPostCodeTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "10001";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        instance.setPostcode(value);
        final Field field = instance.getClass().getDeclaredField("postcode");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setCountryTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Francia";
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        instance.setCountry(value);
        final Field field = instance.getClass().getDeclaredField("country");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getAddress1Test() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("address1");
        field.setAccessible(true);
        field.set(instance, "Avenida Salamanca");

        //when
        final String result = instance.getAddress1();

        //then
        assertEquals("field wasn't retrieved properly", result, "Avenida Salamanca");
    }


    @Test
    public void getAddress2Test() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("address2");
        field.setAccessible(true);
        field.set(instance, "Calle Leo");

        //when
        final Object result = instance.getAddress2();

        //then
        assertEquals("field wasn't retrieved properly", result, "Calle Leo");
    }


    @Test
    public void getCityTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("city");
        field.setAccessible(true);
        field.set(instance, "Caceres");

        //when
        final String result = instance.getCity();

        //then
        assertEquals("field wasn't retrieved properly", result, "Caceres");
    }

    @Test
    public void getStateTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("state");
        field.setAccessible(true);
        field.set(instance, "Extremadura");

        //when
        final String result = instance.getState();

        //then
        assertEquals("field wasn't retrieved properly", result, "Extremadura");
    }

    @Test
    public void getPostCodeTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("postcode");
        field.setAccessible(true);
        field.set(instance, "10001");

        //when
        final String result = instance.getPostcode();

        //then
        assertEquals("field wasn't retrieved properly", result, "10001");
    }

    @Test
    public void getCountryTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Address instance = new es.unex.giis.asee.appdoption.Model.Address();
        final Field field = instance.getClass().getDeclaredField("country");
        field.setAccessible(true);
        field.set(instance, "Francia");

        //when
        final String result = instance.getCountry();

        //then
        assertEquals("field wasn't retrieved properly", result, "Francia");
    }

}
