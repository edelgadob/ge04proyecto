package es.unex.giis.asee.appdoption;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Organization;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class OrganizationUnitTest {
    @Test
    public void setHrefTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "prueba_href";
        Organization instance = new Organization();
        instance.setHref(value);
        final Field field = instance.getClass().getDeclaredField("href");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getHrefTest() throws NoSuchFieldException, IllegalAccessException {
        final Organization instance = new Organization();
        final Field field = instance.getClass().getDeclaredField("href");
        field.setAccessible(true);
        String value = "prueba_href";
        field.set(instance, value);

        //when
        final String result = instance.getHref();

        //then
        assertEquals("field wasn't retrieved properly", result, "prueba_href");
    }
}
