package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

public class PrimaryPhotoCroppedUnitTest {

    @Test
    public void setSmall() throws NoSuchFieldException, IllegalAccessException {
        String value = "Small Photo";
        es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        instance.setSmall(value);
        final Field field = instance.getClass().getDeclaredField("small");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSmall() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        final Field field = instance.getClass().getDeclaredField("small");
        field.setAccessible(true);
        field.set(instance, "Small Photo 1");

        //when
        final String result = instance.getSmall();

        //then
        assertEquals("field wasn't retrieved properly", result, "Small Photo 1");
    }

    @Test
    public void setMedium() throws NoSuchFieldException, IllegalAccessException {
        String value = "Medium Photo";
        es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        instance.setMedium(value);
        final Field field = instance.getClass().getDeclaredField("medium");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getMedium() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        final Field field = instance.getClass().getDeclaredField("medium");
        field.setAccessible(true);
        field.set(instance, "Medium Photo 1");

        //when
        final String result = instance.getMedium();

        //then
        assertEquals("field wasn't retrieved properly", result, "Medium Photo 1");
    }

    @Test
    public void setLarge() throws NoSuchFieldException, IllegalAccessException {
        String value = "Large Photo";
        es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        instance.setLarge(value);
        final Field field = instance.getClass().getDeclaredField("large");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getLarge() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        final Field field = instance.getClass().getDeclaredField("large");
        field.setAccessible(true);
        field.set(instance, "Large Photo 1");

        //when
        final String result = instance.getLarge();

        //then
        assertEquals("field wasn't retrieved properly", result, "Large Photo 1");
    }

    @Test
    public void setFull() throws NoSuchFieldException, IllegalAccessException {
        String value = "Full Photo";
        es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        instance.setFull(value);
        final Field field = instance.getClass().getDeclaredField("full");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getFull() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped instance = new es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped();
        final Field field = instance.getClass().getDeclaredField("full");
        field.setAccessible(true);
        field.set(instance, "Full Photo 1");

        //when
        final String result = instance.getFull();

        //then
        assertEquals("field wasn't retrieved properly", result, "Full Photo 1");
    }
}
